package core;

import java.io.*;
import java.util.*;

import dto.Wyraz;

public class ZadanieRekrutacyjne 
{
	Set<Wyraz> resultTreeSet = new TreeSet<>();
	Map<String,Wyraz> wordList= new HashMap<>();

	public static void main(String[] args) 
	{
		ZadanieRekrutacyjne zadanieRekrutacyjne = new ZadanieRekrutacyjne();
		zadanieRekrutacyjne.populateListsFromFile();
		zadanieRekrutacyjne.printResults();
	}
	
	private void populateListsFromFile()
	{
		int lineNumber = 1;
		boolean isWordUnique;
		
		ClassLoader classLoader = ZadanieRekrutacyjne.class.getClassLoader();
		InputStream inputStream = classLoader.getResourceAsStream("data/sampleText.txt");
		Scanner scannerFile = new Scanner(inputStream);
		
		while (scannerFile.hasNextLine()) 
		{
			String line = scannerFile.nextLine();
			Scanner scannerLine = new Scanner(line);
			scannerLine.useDelimiter("[^\\p{L}]+");
			while(scannerLine.hasNext())
			{
					Wyraz temp = new Wyraz(scannerLine.next(),lineNumber);
					isWordUnique=resultTreeSet.add(temp);
					
					if(isWordUnique)
					{
						wordList.put(temp.getWyraz(),temp);
					}
					else
					{
						wordList.get(temp.getWyraz()).addLinie(lineNumber);
						resultTreeSet.remove(temp);
						resultTreeSet.add(wordList.get(temp.getWyraz()));
					}
					
			}
			scannerLine.close();
			lineNumber++;
		}
		scannerFile.close();
	}
	
	private void printResults()
	{
		for(Wyraz wyraz:resultTreeSet)
		{
			System.out.println(wyraz.getWyraz() + " - " + wyraz.getLinie().size() + " - pozycje -> " + wyraz.getLinie());
		}
	}

}
