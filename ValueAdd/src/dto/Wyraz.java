package dto;

import java.util.ArrayList;

public class Wyraz implements Comparable<Wyraz> 
{
	private String wyraz;
	private ArrayList<Integer> linie = new ArrayList<>();;
	
	public Wyraz(String name, int line)
	{
		setWyraz(name);
		addLinie(line);
	}
	
	public String getWyraz() {
		return wyraz;
	}
	private void setWyraz(String wyraz) {
		this.wyraz = wyraz;
	}
	public ArrayList<Integer> getLinie() {
		return linie;
	}
	public void addLinie(int i) {
		linie.add(i);
	}

	@Override
	public int compareTo(Wyraz o) {
		return this.wyraz.compareTo(o.getWyraz());
	}
}